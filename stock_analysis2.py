""" File: stock_analysis2.py
    Author: Yue Zeng
    Purpose: Visualize Shanghai Stock Exchange (SSE) dataset (index from 2010 to 2020)
    Course: Ista131 Fall 2020
    Instructor: Rich
"""
import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statistics
#import pyfolio as pf
from mplfinance.original_flavor import candlestick_ohlc
from scipy.stats import pearsonr
from matplotlib.dates import DateFormatter, WeekdayLocator, DayLocator, MONDAY, date2num


def daily_return(df):
    
    df_new = pd.DataFrame(index=df.index, columns=['daily_ret', 'intra_ret', 'overnight_ret'])
    for i in range(1,df.shape[0]-1):
        df_new.iloc[i][0] = df.iloc[i][3]/df.iloc[i-1][3] - 1
        df_new.iloc[i][1] = df.iloc[i][3]/df.iloc[i][0] - 1
        df_new.iloc[i][2] = df.iloc[i][0]/df.iloc[i-1][3] 
    return df_new    

def main():
    tickers_list = ['sh_1d', 'sz_1d'] #, 'sh', 'sz'
    data = pd.DataFrame(columns=tickers_list)
    SH_1D = pd.read_csv('sh_1d.csv', index_col = 0)
    data['sh_1d'] = SH_1D.close

    SZ_1D = pd.read_csv('sz_1d.csv', index_col = 0)
    data['sz_1d'] = SZ_1D.close
  
    SH_1D.index.name='date'
    SH_1D = SH_1D[['open','high','low','close','volume']]
    SH_1D.index = pd.to_datetime(SH_1D.index)
    
    SZ_1D.index.name='date'
    SZ_1D = SZ_1D[['open','high','low','close','volume']]
    SZ_1D.index = pd.to_datetime(SZ_1D.index)

    # SSE closing price plot
    
    data['sh_1d'].plot(figsize=(10, 7), color='black')
    plt.title("SSE closing price plot（2010.9.28-2020.9.25）", fontsize=16)
    plt.ylabel('price', fontsize=14)
    plt.xlabel('year', fontsize=14)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    plt.show()


    # Daily rate of return, intraday rate of return, 
    # Mean, standard deviation, median, 25,75 percentile of overnight return

    price=pd.read_csv('sh_1d.csv', index_col=0)
    return_ret = daily_return(price).dropna()
    print(return_ret['daily_ret'].mean())
    np.std(return_ret['daily_ret'])
    np.median(return_ret['daily_ret'])
    np.percentile(return_ret['daily_ret'],25)
    np.percentile(return_ret['daily_ret'],75)
    print(return_ret['intra_ret'].mean())
    print(return_ret['overnight_ret'].mean())
    
    # Mean of positive daily return rate
    # Mean of negative daily return rate

    daily_ret_pos = []
    daily_ret_neg = []

    for i in range(1,return_ret.shape[0]-1):
        if return_ret['daily_ret'].iloc[i] > 0:
            daily_ret_pos.append(return_ret['daily_ret'].iloc[i])
        else:
            daily_ret_neg.append(return_ret['daily_ret'].iloc[i])
    print(np.mean(daily_ret_pos), np.mean(daily_ret_neg))
    
    # Boxplot of Daily rate of return, Intraday rate of return, Overnight rate of return
    
    plt.boxplot(return_ret['daily_ret'])  
    plt.show()
    
    plt.boxplot(return_ret['intra_ret'])  
    plt.show()
    
    plt.boxplot(return_ret['overnight_ret'])  
    plt.show()



    x = SH_1D['volume']
    y = SH_1D['close']
    SH_1D.plot(x='volume',y='close',kind='scatter',color = 'g')
    plt.xlabel('Volume')
    plt.ylabel('Stock price')
    plt.title('Stock price and Volume')
    plt.grid(True)
    m, b = np.polyfit(x, y, 1)
    plt.plot(x, m*x + b)
    plt.show()


if __name__ == "__main__":
    main()