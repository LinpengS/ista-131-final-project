# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 08:50:22 2020

@author: Linpeng
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyfolio as pf
import statistics
from mplfinance.original_flavor import candlestick_ohlc
from scipy.stats import pearsonr
from matplotlib.dates import DateFormatter, WeekdayLocator, DayLocator, MONDAY, date2num
from statistics import mean 
import statsmodels.api as sm
from statsmodels.formula.api import ols

# 添加中文显示
plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

# 定义绘制K线图的函数
def pandas_candlestick_ohlc(stock_data, otherseries=None):
    # 设置绘图参数，主要是坐标轴
    mondays = WeekdayLocator(MONDAY)
    alldays = DayLocator()
    dayFormatter = DateFormatter('%d')
 
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2)
    
    if stock_data.index[-1] - stock_data.index[0] < pd.Timedelta('730 days'):
        weekFormatter = DateFormatter('%b %d')
        ax.xaxis.set_major_locator(mondays)
        ax.xaxis.set_minor_locator(alldays)
    else:
        weekFormatter = DateFormatter('%b %d, %Y')
    #weekFormatter = DateFormatter('%b %d, %Y')
    ax.xaxis.set_major_formatter(weekFormatter)
    ax.grid(True)
 
    # 创建K线图
    stock_array = np.array(stock_data.reset_index()[['date','open','high','low','close']])
    stock_array[:,0] = date2num(stock_array[:,0])
    candlestick_ohlc(ax, stock_array, colorup = "red", colordown="green", width=0.6)
 
    # 可同时绘制其他折线图
    if otherseries is not None:
        for each in otherseries:
            plt.plot(stock_data[each], label=each)
        plt.legend()
 
    ax.xaxis_date()
    ax.autoscale_view()
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
    plt.ylabel('Price')#指数
    plt.show()

# t日均线技术
def t_day_return(t, price):
    
    ret_above_t = []
    ret_below_t = []

    for i in range(t,price.shape[0]-1):
        ma_t = price['close'].iloc[i-(t-1):i+1].mean()
        close = price['close'].iloc[i]
        above_ma_t = close > ma_t
        below_ma_t = close <= ma_t
        if above_ma_t:
            ret_above_t.append(price['close'].iloc[i+1]/price['close'].iloc[i]-1)
        if below_ma_t:
            ret_below_t.append(price['close'].iloc[i+1]/price['close'].iloc[i]-1)            
    
    return ret_above_t, ret_below_t
   
def daily_return(df):
    
    df_new = pd.DataFrame(index=df.index, columns=['daily_ret', 'intra_ret', 'overnight_ret'])
    for i in range(1,df.shape[0]-1):
        df_new.iloc[i][0] = df.iloc[i][3]/df.iloc[i-1][3] - 1
        df_new.iloc[i][1] = df.iloc[i][3]/df.iloc[i][0] - 1
        df_new.iloc[i][2] = df.iloc[i][0]/df.iloc[i-1][3] 
    return df_new
    
def main():
    tickers_list = ['sh_1d', 'sz_1d'] #, 'sh', 'sz'
    data = pd.DataFrame(columns=tickers_list)
    SH_1D = pd.read_csv('E:\School\ISTA 131\Final Project/sh_1d.csv', index_col = 0)
    data['sh_1d'] = SH_1D.close
    
    SZ_1D = pd.read_csv('E:\School\ISTA 131\Final Project/sz_1d.csv', index_col = 0)
    data['sz_1d'] = SZ_1D.close
    
    #print(data)
    #print(data.shape)
    
    SH_1D.index.name='date'
    SH_1D = SH_1D[['open','high','low','close','volume']]
    SH_1D.index = pd.to_datetime(SH_1D.index)
    
    # SZ_1D.index.name='date'
    # SZ_1D = SZ_1D[['open','high','low','close','volume']]
    # SZ_1D.index = pd.to_datetime(SZ_1D.index)

#---------------------------------------------------------------------------------------------     
    '''
    # 上证收盘价plot
    data['sh_1d'].plot(figsize=(10, 7), color='black')
    plt.title("Shanghai Close Price（2010.9.28-2020.9.25）", fontsize=16)#上证收盘价
    plt.ylabel('price', fontsize=14)#价格
    plt.xlabel('year', fontsize=14)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    plt.show()
    
    # 深证收盘价plot
    data['sz_1d'].plot(figsize=(10, 7), color='black')
    plt.title("深证收盘价（2010.9.28-2020.9.25）", fontsize=16)
    plt.ylabel('价格', fontsize=14)
    plt.xlabel('年', fontsize=14)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    plt.show()
    '''
#---------------------------------------------------------------------------------------------     
    '''
    # 上证月平均收盘价plot
    SH_1D_2 = SH_1D.assign(year = SH_1D.index.strftime('%Y').astype(int), month = SH_1D.index.strftime('%m').astype(int), day = SH_1D.index.strftime('%d').astype(int))
    SH_1D_group = SH_1D_2.groupby(['year', 'month'])
    SH_1D_monthly_averages = SH_1D_group.aggregate({"close":np.mean})
    SH_1D_monthly_averages.plot(figsize=(10, 7), color = 'blue')
    plt.title("Shanghai Monthly Average Close Price 2010.9-2020.9", fontsize=16)#上证月平均收盘价
    plt.ylabel('price', fontsize=14)#价格
    plt.xlabel('time', fontsize=14)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=1.0)
    plt.legend(['monthly average close price'])#月平均收盘价
    plt.show()
    
    # 深证月平均收盘价plot
    SZ_1D_2 = SZ_1D.assign(year = SZ_1D.index.strftime('%Y').astype(int), month = SZ_1D.index.strftime('%m').astype(int), day = SZ_1D.index.strftime('%d').astype(int))
    SZ_1D_group = SZ_1D_2.groupby(['year', 'month'])
    SZ_1D_monthly_averages = SZ_1D_group.aggregate({"close":np.mean})
    SZ_1D_monthly_averages.plot(figsize=(10, 7), color = 'blue')
    plt.title("深证月平均收盘价 2010.9-2020.9", fontsize=16)
    plt.ylabel('价格', fontsize=14)
    plt.xlabel('时间', fontsize=14)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=1.0)
    plt.legend(['月平均收盘价'])
    plt.show()
    '''
#--------------------------------------------------------------------------------------------- 
    '''
    # 移动平均线
    # 上证
    SH_1D['5_day'] = np.round(SH_1D['close'].rolling(window=5,center=False).mean(),2)
    SH_1D['20_day'] = np.round(SH_1D['close'].rolling(window=20,center=False).mean(),2)
    SH_1D_2010 = SH_1D.loc['2020-01-01':'2020-09-25']
    pandas_candlestick_ohlc(SH_1D_2010,['5_day','20_day'])
    
    # 深证
    SZ_1D['5_day'] = np.round(SZ_1D['close'].rolling(window=5,center=False).mean(),2)
    SZ_1D['20_day'] = np.round(SZ_1D['close'].rolling(window=20,center=False).mean(),2)
    SZ_1D_2010 = SZ_1D.loc['2020-01-01':'2020-09-25']
    pandas_candlestick_ohlc(SZ_1D_2010,['5_day','20_day'])
    '''
#---------------------------------------------------------------------------------------------     
    '''
    # 收盘价连续增涨
    increase = []
    #print(data['sh_1d'].head())
    #for index, values in data['sh_1d'].items():
        
        #if abs(data['sh_1d'][index+1]) > abs(data['sh_1d'][index]):
            #increase.append(index+1)
    for i in range(len(data['sh_1d'])-1):
        if data['sh_1d'][i+1] > data['sh_1d'][i]:
            increase.append(data['sh_1d'][i+1])
    
    print(increase)
    '''
#---------------------------------------------------------------------------------------------     
    
    # 股票数据
    #print(SH_1D.info())
    #print(SZ_1D)
    
#---------------------------------------------------------------------------------------------     
    '''
    # 绘制股票收盘价和成交量的时间序列图
    # 上证
    SH_1D.index.name = 'date'
    new_df = SH_1D[800:1200]
    new_df[['close','volume']].plot(secondary_y='volume',grid=True)
    plt.title('Shanghai 2014.1-2015.9 Close Price & Volume', fontsize='9')#上证2010.9-2020.9收盘价和成交量的时间序列图
    plt.xlabel('time', fontsize=14)#时间
    #plt.legend()
    plt.show()
    
    # 深证
    SZ_1D.index.name = 'date'
    SZ_1D[['close','volume']].plot(secondary_y='volume',grid=True)
    plt.title('深证2010.9-2020.9收盘价和成交量的时间序列图', fontsize='9')
    plt.xlabel('时间', fontsize=14)
    plt.legend()
    plt.show()
    '''
#---------------------------------------------------------------------------------------------     
    '''
    # 绘制上证K线图
    SH_1D.index.name='date'
    SH_1D = SH_1D[['open','high','low','close','volume']]
    SH_1D.index = pd.to_datetime(SH_1D.index)
    pandas_candlestick_ohlc(SH_1D[800:900])
    
    # 绘制深证K线图
    SZ_1D.index.name='date'
    SZ_1D = SZ_1D[['open','high','low','close','volume']]
    SZ_1D.index = pd.to_datetime(SZ_1D.index)
    pandas_candlestick_ohlc(SZ_1D)
    '''
#---------------------------------------------------------------------------------------------    
    '''
    # t日股价收盘价
    price=pd.read_csv('E:\School\ISTA 131\Final Project/sh_1d.csv', index_col=0)
    value_1, value_2 = t_day_return(5, price)
    # print(np.mean(value_1))
    # print(np.mean(value_2))
    # print(np.percentile(value_1,25))
    # print(np.percentile(value_1,75))
    plt.hist(value_2, bins = 100)
    plt.xlabel("5-day close price <ma(t,5)")
    plt.show()
    '''
#---------------------------------------------------------------------------------------------  
    '''
    # 每日回报率，日内回报率，隔夜回报率的均值，标准差，中位数，25,75百分位数
    return_ret = daily_return(price).dropna()
    print(return_ret['daily_ret'].mean())
    np.std(return_ret['daily_ret'])
    np.median(return_ret['daily_ret'])
    np.percentile(return_ret['daily_ret'],25)
    np.percentile(return_ret['daily_ret'],75)
    print(return_ret['intra_ret'].mean())
    print(return_ret['overnight_ret'].mean())
    
    # 每日回报率为正数时的均值，为负数时的均值 
    daily_ret_pos = []
    daily_ret_neg = []
    for i in range(1,return_ret.shape[0]-1):
        if return_ret['daily_ret'].iloc[i] > 0:
            daily_ret_pos.append(return_ret['daily_ret'].iloc[i])
        else:
            daily_ret_neg.append(return_ret['daily_ret'].iloc[i])
    print(mean(daily_ret_pos), mean(daily_ret_neg))
    
    # 每日回报率，日内回报率，隔夜回报率的boxplot
    
    plt.boxplot(return_ret['daily_ret'])  
    plt.show()
    
    plt.boxplot(return_ret['intra_ret'])  
    plt.show()
    
    plt.boxplot(return_ret['overnight_ret'])  
    plt.show()
    '''
#--------------------------------------------------------------------------------------------- 
         
    # 定义并增加分型(type)到df SH_1D, SZ_1D
    SH_1D['stock_type'] = ""
    SH_1D['parting_type'] = ""
    for i in range(1, SH_1D.shape[0]):
        if SH_1D['high'].iloc[i] > SH_1D['high'].iloc[i-1] and  SH_1D['low'].iloc[i] > SH_1D['low'].iloc[i-1]:
            SH_1D.iat[i, 5] = 'rise' # 上涨分型
        elif SH_1D['high'].iloc[i] < SH_1D['high'].iloc[i-1] and  SH_1D['low'].iloc[i] < SH_1D['low'].iloc[i-1]:
            SH_1D.iat[i, 5] = 'drop' # 下跌分型
    for i in range(1, SH_1D.shape[0]):
        if SH_1D['stock_type'].iloc[i-1] == 'drop':
            SH_1D.iat[i, 6] = 'bottom' # 底分型
        elif SH_1D['stock_type'].iloc[i-1] == 'rise':
            SH_1D.iat[i, 6] = 'top' # 顶分型
    '''        
    SZ_1D['stock_type'] = "" # 上涨,下跌分型
    SZ_1D['parting_type'] = "" # 底分型,顶分型
    for i in range(1, SZ_1D.shape[0]):
        if SZ_1D['high'].iloc[i] > SZ_1D['high'].iloc[i-1] and  SZ_1D['low'].iloc[i] > SZ_1D['low'].iloc[i-1]:
            SZ_1D.iat[i, 5] = 'rise' # 上涨分型
        elif SZ_1D['high'].iloc[i] < SZ_1D['high'].iloc[i-1] and  SZ_1D['low'].iloc[i] < SZ_1D['low'].iloc[i-1]:
            SZ_1D.iat[i, 5] = 'drop' # 下跌分型
    for i in range(1, SZ_1D.shape[0]):
        if SZ_1D['stock_type'].iloc[i-1] == 'drop' and SZ_1D['stock_type'].iloc[i] == 'rise':
            SZ_1D.iat[i, 6] = 'bottom' # 底分型
        elif SZ_1D['stock_type'].iloc[i-1] == 'rise' and SZ_1D['stock_type'].iloc[i] == 'drop':
            SZ_1D.iat[i, 6] = 'top' # 顶分型
    '''
#--------------------------------------------------------------------------------------------- 
    '''        
    # 研究分型对股价影响
    return_ret = daily_return(SH_1D).dropna()
    
    # t日为上涨分型时
    t_rise_daily_ret = [] 
    t_rise_intra_ret = [] 
    t_rise_overnight_ret = []
    
    # t日为下跌分型时
    t_drop_daily_ret = [] 
    t_drop_intra_ret = [] 
    t_drop_overnight_ret = []

    for i in range(1, SH_1D.shape[0]-2):
        if SH_1D['stock_type'].iloc[i] == 'drop':
            t_drop_daily_ret.append(float(return_ret['daily_ret'].iloc[i]))
            t_drop_intra_ret.append(float(return_ret['intra_ret'].iloc[i]))
            t_drop_overnight_ret.append(float(return_ret['overnight_ret'].iloc[i-1]))
        elif SH_1D['stock_type'].iloc[i] == 'rise':
            t_rise_daily_ret.append(float(return_ret['daily_ret'].iloc[i]))
            t_rise_intra_ret.append(float(return_ret['intra_ret'].iloc[i]))
            t_rise_overnight_ret.append(float(return_ret['overnight_ret'].iloc[i-1]))
            
    # print("mean: " + str(round(mean(t_rise_daily_ret), 8)) + "\n std: " + str(round(statistics.stdev(t_rise_daily_ret), 8)) + "\n 25: " + str(round(np.percentile(t_rise_daily_ret, 25), 8)) + "\n 75: " + str(round(np.percentile(t_rise_daily_ret, 75), 8)))
    # print("mean: " + str(round(mean(t_rise_intra_ret), 8)) + "\n std: " + str(round(statistics.stdev(t_rise_intra_ret), 8)) + "\n 25: " + str(round(np.percentile(t_rise_intra_ret, 25), 8)) + "\n 75: " + str(round(np.percentile(t_rise_intra_ret, 75), 8)))
    # print("mean: " + str(round(mean(t_rise_overnight_ret), 8)) + "\n std: " + str(round(statistics.stdev(t_rise_overnight_ret), 8)) + "\n 25: " + str(round(np.percentile(t_rise_overnight_ret, 25), 8)) + "\n 75: " + str(round(np.percentile(t_rise_overnight_ret, 75), 8)))
    
    # t日为底分型时
    t_b_d_r = []
    t_b_i_r = []
    t_b_o_r = []
    
    # t日为顶分型时
    t_t_d_r = []
    t_t_i_r = []
    t_t_o_r = []
    
    for i in range(1, SH_1D.shape[0]-2):
        if SH_1D['parting_type'].iloc[i] == 'bottom':
            t_b_d_r.append(float(return_ret['daily_ret'].iloc[i]))
            t_b_i_r.append(float(return_ret['intra_ret'].iloc[i]))
            t_b_o_r.append(float(return_ret['overnight_ret'].iloc[i-1]))
        elif SH_1D['parting_type'].iloc[i] == 'top':
            t_t_d_r.append(float(return_ret['daily_ret'].iloc[i]))
            t_t_i_r.append(float(return_ret['intra_ret'].iloc[i]))
            t_t_o_r.append(float(return_ret['overnight_ret'].iloc[i-1]))
    
    # print("mean: " + str(round(mean(t_b_d_r), 8)) + "\n std: " + str(round(statistics.stdev(t_b_d_r), 8)) + "\n 25: " + str(round(np.percentile(t_b_d_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_b_d_r, 75), 8)))
    # print("mean: " + str(round(mean(t_b_i_r), 8)) + "\n std: " + str(round(statistics.stdev(t_b_i_r), 8)) + "\n 25: " + str(round(np.percentile(t_b_i_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_b_i_r, 75), 8)))
    # print("mean: " + str(round(mean(t_b_o_r), 8)) + "\n std: " + str(round(statistics.stdev(t_b_o_r), 8)) + "\n 25: " + str(round(np.percentile(t_b_o_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_b_o_r, 75), 8)))
    
    # print("mean: " + str(round(mean(t_t_d_r), 8)) + "\n std: " + str(round(statistics.stdev(t_t_d_r), 8)) + "\n 25: " + str(round(np.percentile(t_t_d_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_t_d_r, 75), 8)))
    # print("mean: " + str(round(mean(t_t_i_r), 8)) + "\n std: " + str(round(statistics.stdev(t_t_i_r), 8)) + "\n 25: " + str(round(np.percentile(t_t_i_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_t_i_r, 75), 8)))
    # print("mean: " + str(round(mean(t_t_o_r), 8)) + "\n std: " + str(round(statistics.stdev(t_t_o_r), 8)) + "\n 25: " + str(round(np.percentile(t_t_o_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_t_o_r, 75), 8)))
    
    # 上涨，下跌分型，底分型，顶分型出现的频率
    print(round(len(t_rise_daily_ret)/2433, 6))
    print(round(len(t_drop_daily_ret)/2433, 6))
    print(round(len(t_b_d_r)/2433, 6))
    print(round(len(t_t_d_r)/2433, 6))
    '''
#--------------------------------------------------------------------------------------------- 
    '''
    # 成交量(volume)对股价的影响
    return_ret = daily_return(SH_1D).dropna()
    
    # t日成交量大于t-1日
    t_v_g_d_r = [] 
    t_v_g_i_r = [] 
    t_v_g_o_r = []
    
    # t日成交量大于t-1日1.1倍时
    t_v_g_d_r_11 = [] 
    t_v_g_i_r_11 = [] 
    t_v_g_o_r_11 = []

    # t日成交量大于t-1日1.2倍时
    t_v_g_d_r_12 = [] 
    t_v_g_i_r_12 = [] 
    t_v_g_o_r_12 = []

    # t日成交量小于t-1日
    t_v_s_d_r = [] 
    t_v_s_i_r = [] 
    t_v_s_o_r = []
    
    # t日成交量小于t-1日0.9倍时
    t_v_s_d_r_09 = [] 
    t_v_s_i_r_09 = [] 
    t_v_s_o_r_09 = []

    # t日成交量小于t-1日0.8倍时
    t_v_s_d_r_08 = [] 
    t_v_s_i_r_08 = [] 
    t_v_s_o_r_08 = []
    
    for i in range(1, SH_1D.shape[0]-2):
        if SH_1D['volume'].iloc[i] > SH_1D['volume'].iloc[i-1]:
            t_v_g_d_r.append(float(return_ret['daily_ret'].iloc[i]))
            t_v_g_i_r.append(float(return_ret['intra_ret'].iloc[i]))
            t_v_g_o_r.append(float(return_ret['overnight_ret'].iloc[i-1]))
            if SH_1D['volume'].iloc[i] > SH_1D['volume'].iloc[i-1]*1.2:
                t_v_g_d_r_12.append(float(return_ret['daily_ret'].iloc[i]))
                t_v_g_i_r_12.append(float(return_ret['intra_ret'].iloc[i]))
                t_v_g_o_r_12.append(float(return_ret['overnight_ret'].iloc[i-1]))
            elif SH_1D['volume'].iloc[i] > SH_1D['volume'].iloc[i-1]*1.1:
                t_v_g_d_r_11.append(float(return_ret['daily_ret'].iloc[i]))
                t_v_g_i_r_11.append(float(return_ret['intra_ret'].iloc[i]))
                t_v_g_o_r_11.append(float(return_ret['overnight_ret'].iloc[i-1]))
        elif SH_1D['volume'].iloc[i] < SH_1D['volume'].iloc[i-1]:
            t_v_s_d_r.append(float(return_ret['daily_ret'].iloc[i]))
            t_v_s_i_r.append(float(return_ret['intra_ret'].iloc[i]))
            t_v_s_o_r.append(float(return_ret['overnight_ret'].iloc[i-1]))
            if SH_1D['volume'].iloc[i] < SH_1D['volume'].iloc[i-1]*0.8:
                t_v_s_d_r_08.append(float(return_ret['daily_ret'].iloc[i]))
                t_v_s_i_r_08.append(float(return_ret['intra_ret'].iloc[i]))
                t_v_s_o_r_08.append(float(return_ret['overnight_ret'].iloc[i-1]))
            elif SH_1D['volume'].iloc[i] < SH_1D['volume'].iloc[i-1]*0.9:
                t_v_s_d_r_09.append(float(return_ret['daily_ret'].iloc[i]))
                t_v_s_i_r_09.append(float(return_ret['intra_ret'].iloc[i]))
                t_v_s_o_r_09.append(float(return_ret['overnight_ret'].iloc[i-1]))
                
    # print("mean: " + str(round(mean(t_v_g_d_r), 8)) + "\n std: " + str(round(statistics.stdev(t_v_g_d_r), 8)) + "\n 25: " + str(round(np.percentile(t_v_g_d_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_v_g_d_r, 75), 8)))
    # print("mean: " + str(round(mean(t_v_g_i_r), 8)) + "\n std: " + str(round(statistics.stdev(t_v_g_i_r), 8)) + "\n 25: " + str(round(np.percentile(t_v_g_i_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_v_g_i_r, 75), 8)))
    # print("mean: " + str(round(mean(t_v_g_o_r), 8)) + "\n std: " + str(round(statistics.stdev(t_v_g_o_r), 8)) + "\n 25: " + str(round(np.percentile(t_v_g_o_r, 25), 8)) + "\n 75: " + str(round(np.percentile(t_v_g_o_r, 75), 8)))
    
    # print("1.1:\n mean: " + str(round(mean(t_v_g_d_r_11), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_d_r_11), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_d_r_11, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_d_r_11, 75), 6)))
    # print("mean: " + str(round(mean(t_v_g_i_r_11), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_i_r_11), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_i_r_11, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_i_r_11, 75), 6)))
    # print("mean: " + str(round(mean(t_v_g_o_r_11), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_o_r_11), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_o_r_11, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_o_r_11, 75), 6)))
    
    # print("1.2:\n mean: " + str(round(mean(t_v_g_d_r_12), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_d_r_12), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_d_r_12, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_d_r_12, 75), 6)))
    # print("mean: " + str(round(mean(t_v_g_i_r_12), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_i_r_12), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_i_r_12, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_i_r_12, 75), 6)))
    # print("mean: " + str(round(mean(t_v_g_o_r_12), 6)) + "\n std: " + str(round(statistics.stdev(t_v_g_o_r_12), 6)) + "\n 25: " + str(round(np.percentile(t_v_g_o_r_12, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_g_o_r_12, 75), 6)))
    
    # print("mean: " + str(round(mean(t_v_s_d_r), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_d_r), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_d_r, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_d_r, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_i_r), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_i_r), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_i_r, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_i_r, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_o_r), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_o_r), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_o_r, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_o_r, 75), 6)))
    
    # print("0.9:\n mean: " + str(round(mean(t_v_s_d_r_09), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_d_r_09), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_d_r_09, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_d_r_09, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_i_r_09), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_i_r_09), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_i_r_09, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_i_r_09, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_o_r_09), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_o_r_09), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_o_r_09, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_o_r_09, 75), 6)))
    
    # print("0.8:\n mean: " + str(round(mean(t_v_s_d_r_08), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_d_r_08), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_d_r_08, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_d_r_08, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_i_r_08), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_i_r_08), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_i_r_08, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_i_r_08, 75), 6)))
    # print("mean: " + str(round(mean(t_v_s_o_r_08), 6)) + "\n std: " + str(round(statistics.stdev(t_v_s_o_r_08), 6)) + "\n 25: " + str(round(np.percentile(t_v_s_o_r_08, 25), 6)) + "\n 75: " + str(round(np.percentile(t_v_s_o_r_08, 75), 6)))
    
    # t日成交量大于t-1日出现的频率
    print(round(len(t_v_g_d_r)/2433, 6)*100)
    print(round(len(t_v_g_d_r_11)/2433, 6)*100)
    print(round(len(t_v_g_d_r_12)/2433, 6)*100)
    
    # t日成交量小于t-1日出现的频率
    print(round(len(t_v_s_d_r)/2433, 6)*100)
    print(round(len(t_v_s_d_r_09)/2433, 6)*100)
    print(round(len(t_v_s_d_r_08)/2433, 6)*100)
    '''
#--------------------------------------------------------------------------------------------- 
    '''
    # Relationship between |volume| and |close|
    SH_1D['abs_close'] = ""
    SH_1D['abs_volume'] = ""
    for i in range(1, SH_1D.shape[0]):
        SH_1D.iat[i, 7] = abs(SH_1D['close'].iloc[i]-SH_1D['close'].iloc[i-1])
        SH_1D.iat[i, 8] = abs(SH_1D['volume'].iloc[i]-SH_1D['volume'].iloc[i-1])
    abs_Close = SH_1D['abs_close'].tolist()
    abs_Close.pop(0)
    abs_Close.sort()
    abs_Volume = SH_1D['abs_volume'].tolist()
    abs_Volume.pop(0)
    abs_Volume.sort()
    
    plt.plot(abs_Volume, abs_Close)
    plt.show()
    plt.title("|delta Volume| vs |delta Close|", fontsize=16)
    plt.ylabel('|delta Close|', fontsize=14)
    plt.xlabel('|delta Volume|', fontsize=14)
    plt.show()
    '''
    # model = sm.OLS(abs_Close, abs_Volume).fit()
    # predictions = model.predict(abs_Volume) # make the predictions by the model

    # Print out the statistics
    #print(model.summary())

#---------------------------------------------------------------------------------------------     
if __name__ == "__main__":
    main()